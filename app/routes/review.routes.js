var review = require('../controller/review.controller')
var Authentication = require('../middleware/Authentication')
var Authorization = require('../middleware/Authorization')

module.exports =(app)=>{
    app.post('/api/review/', Authentication, review.reviewCreate)
    app.get('/api/review', review.reviewShowAll)
    app.get('/api/review/:id', review.reviewShow)
    app.put('/api/review/:id',Authentication, Authorization.review, review.reviewUpdate )
    app.delete('/api/review/:id', Authentication, Authorization.review, review.reviewDelete)

}