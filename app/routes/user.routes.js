var user = require('../controller/user.controller')
var CheckUser = require('../middleware/CheckUser')
var Authentication = require('../middleware/Authentication')
var Authorization = require('../middleware/Authorization')

module.exports = (app) =>{
    app.post('/api/user', CheckUser.create, user.userCreate)
    app.get('/api/user', user.userShowAll)
    app.get('/api/user/:id', user.userShow)
    app.put('/api/user/:id', Authentication,Authorization.user, CheckUser.update, user.userUpdate)
    app.delete('/api/user/:id', user.userDelete)
    app.post('/api/user/login', user.userLogin)
    
}