var product = require('../controller/product.controller')
var Authentication = require('../middleware/Authentication')
var Authorization = require('../middleware/Authorization')


module.exports =(app)=>{
    app.post('/api/product', Authentication, product.productCreate )
    app.get('/api/product', product.productShowAll)
    app.get('/api/product/:id', product.productShow)
    app.delete('/api/product/:id', Authentication, Authorization.product, product.productDelete)
    app.put('/api/product/:id', Authentication, Authorization.product, product.productUpdate )
}