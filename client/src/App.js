import React from 'react';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom'
import Login from './User/Login.js'
import SignUp from './User/SignUp.js'
import UserShow from './User/UserShow.js'
import UserAll from './User/UserAll.js'
import Header from './Header.js'
import Product from './Product/ProductForm'
import  Review from './Review/CreateReview'
import './App.css'
import Home from './Page/Home'

function App() {
  return (
    <div >
      <Router>
        <Header />
        <Route exact path='/' component={Home}></Route>
        <Route exact path='/login' component={Login}></Route>
        <Route exact path='/signup' component={SignUp}></Route>
        <Route exact path='/user/:id' component={UserShow}></Route>
        <Route exact path='/user' component={UserAll}></Route>
        <Route exact path='/productCreate' component={Product}></Route>
        <Route exact path='/product-review' component={Review}></Route>

      </Router>
    </div>
  );
}

export default App;
