import React from 'react';
import { withRouter, Redirect } from 'react-router-dom'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Modal, ModalHeader, ModalBody, ModalFooter, Button, Form, FormGroup, Label, Input
  
} from 'reactstrap';
import { createRequireFromPath } from 'module';


class Header extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false,
      modal: false,
      username: '',
      password: '',
      userId: '',
      redirect: false
    }
    this.logout = this.logout.bind(this)
    this.btntoggle = this.btntoggle.bind(this)
    this.onChangeUsername = this.onChangeUsername.bind(this)
    this.onChangePassword = this.onChangePassword.bind(this)
    this.login = this.login.bind(this)
    
  }
  login() {
    fetch('https://week34.herokuapp.com/api/user/login', {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: 'follow', // manual, *follow, error
      referrer: 'no-referrer', // no-referrer, *client
      body: JSON.stringify({
        username: this.state.username,
        password: this.state.password
      }) // body data type must match "Content-Type" header
    }).then(response => response.json())
      .then(data => {
        // store.set('isLoggedIn', true)
        localStorage.setItem('isLoggedIn', true)
        localStorage.setItem('TOKEN', data.data.token)
        localStorage.setItem('USER', data.data.userId)


      }).then(() => {
         this.props.history.push('/')
         this.setState(prevState => ({
          modal: !prevState.modal
        }));

      })
  }

  onChangeUsername(event) {
    this.setState({
      username: event.target.value
    })
  }
  onChangePassword(event) {
    this.setState({
      password: event.target.value
    })
  }
  btntoggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }
  logout() {
    localStorage.clear();
    this.props.history.push('/')
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    var SignupCheck
    var LoginCheck
    if (localStorage.getItem('isLoggedIn')) {
      LoginCheck =
        <NavItem onClick={this.logout}>
          <NavLink className="link-white">Logout</NavLink>
        </NavItem>
      SignupCheck =<div></div>

    } else {
      LoginCheck =
        <NavItem>
          <NavLink className="link-white" onClick={this.btntoggle} href="#">Login</NavLink>
        </NavItem>
      SignupCheck = 
      <NavItem>
      <NavLink className="link-white" href="/signup">Sign Up</NavLink>
    </NavItem>
  }

      return (
        <div>
          <Navbar className="navbar-css" expand="md">
            <NavbarBrand className="link-white geser" href="/"><img className="logoku" src={require('./Asset/logoku.png')}/><h7> Cyluck Comunity</h7></NavbarBrand>
            <NavbarToggler onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="ml-auto" navbar>
                {LoginCheck}
                {SignupCheck}
                <UncontrolledDropdown nav inNavbar>
                  <DropdownToggle className="link-white" nav caret>
                    Options
                </DropdownToggle>
                  <DropdownMenu right>
                    <DropdownItem>
                      <NavLink href="/user">Users</NavLink>
                    </DropdownItem>
                    <DropdownItem>
                      <NavLink href="/productCreate"> Create Product</NavLink>
                  </DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem>
                      Reset
                  </DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
              </Nav>
            </Collapse>
          </Navbar>
          <div>
       
        <Modal isOpen={this.state.modal} toggle={this.btntoggle} className={this.props.className}>
          <ModalHeader toggle={this.btntoggle}>Sign In</ModalHeader>
          <ModalBody>
          <Form inline>
            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
              <Label for="exampleEmail" className="mr-sm-2">Username</Label>
              <Input onChange={this.onChangeUsername} type="text" name="email" id="exampleEmail" placeholder="username" />
            </FormGroup>
            <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
              <Label for="examplePassword" className="mr-sm-2">Password</Label>
              <Input onChange={this.onChangePassword} type="password" name="password" id="examplePassword" placeholder="don't tell!" />
            </FormGroup>
            
          </Form>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.login}>Login</Button>{' '}
            <Button color="secondary" onClick={this.btntoggle}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
        </div>
        
      );
    }
  }

export default withRouter(Header)
