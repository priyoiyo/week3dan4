import React from 'react';
import { withRouter } from 'react-router-dom'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  
} from 'reactstrap';
import { createRequireFromPath } from 'module';


class Header extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    }
    this.logout = this.logout.bind(this)
  }

  logout() {
    localStorage.clear();
    this.props.history.push('/')
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    var SignupCheck
    var LoginCheck
    if (localStorage.getItem('isLoggedIn')) {
      LoginCheck =
        <NavItem onClick={this.logout}>
          <NavLink className="link-white">Logout</NavLink>
        </NavItem>
      SignupCheck =<div></div>

    } else {
      LoginCheck =
        <NavItem>
          <NavLink className="link-white" href="/login">Login</NavLink>
        </NavItem>
      SignupCheck = 
      <NavItem>
      <NavLink className="link-white" href="/signup">Sign Up</NavLink>
    </NavItem>
  }

      return (
        <div>
          <Navbar className="navbar-css" expand="md">
            <NavbarBrand className="link-white geser" href="/"><img className="logoku" src={require('./Asset/logoku.png')}/><h7> Cyluck Comunity</h7></NavbarBrand>
            <NavbarToggler onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="ml-auto" navbar>
                {LoginCheck}
                {SignupCheck}
                <UncontrolledDropdown nav inNavbar>
                  <DropdownToggle className="link-white" nav caret>
                    Options
                </DropdownToggle>
                  <DropdownMenu right>
                    <DropdownItem>
                      <NavLink href="/user">Users</NavLink>
                    </DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
              </Nav>
            </Collapse>
          </Navbar>
        </div>
      );
    }
  }

export default withRouter(Header)
