import React, { createRef } from 'react';
import { Col, Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import ReactDOM from 'react-dom';

import axios from 'axios'
import { Redirect } from 'react-router-dom'



export default class Example extends React.Component {
  // This function does the uploading to cloudinary
  constructor(props) {
    super(props);
    this.state = {
      review: ""
    }
    this.onChangeReview = this.onChangeReview.bind(this)
    

  }

  createReview(event) {
    event.preventDefault()
    fetch('https://week34.herokuapp.com/api/review', {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem('TOKEN')
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: 'follow', // manual, *follow, error
      referrer: 'no-referrer', // no-referrer, *client
      body: JSON.stringify({
        review: this.state.review
      }) // body data type must match "Content-Type" header
    }).then(response => response.json())
      .then(data => {
        console.log('Review CREATED', data)
        this.props.history.push('/')
      })
  }
  onChangeReview(event) {
    this.setState({
      review: event.target.value
    })
  }
  

  
  
  render() {
    
      return (

        <Form>
          
          <FormGroup row>
            <Label for="reviewBarang" sm={2}>Review Barang</Label>
            <Col>
              <Input type="textarea" name="text" id="text" placeholder="Review Barang" />
            </Col>
          </FormGroup>
          

          <FormGroup check row>
            <Col sm={{ size: 10, offset: 2 }}>
              <Button>Submit</Button>
            </Col>
          </FormGroup>
        </Form>
      
      );
      // console.log('asdasd'))
    }
   

  
}
