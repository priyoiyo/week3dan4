require('dotenv').config()
const express = require('express');
const app = express();
const path = require('path');
var cors = require('cors')
var PORT = process.env.PORT || 5000
const bodyParser = require('body-parser')
const config_server = process.env.DB_ATLAS_MONGO || process.env.DB_LOCAL_MONGO

//Install Cors untuk yg nge block fetch.
app.use(cors())

const mongoose = require('mongoose');
mongoose.connect(config_server, {useNewUrlParser: true, useUnifiedTopology: true
}).then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});



// Serve the static files from the React app
app.use(express.static(path.join(__dirname, 'client/build')));

app.use(bodyParser.json())

//API
require('./app/routes/user.routes')(app)
require('./app/routes/products.routes')(app)
require('./app/routes/review.routes')(app)

// An api endpoint that returns a short list of items
app.get('/api/getList', (req,res) => {
    var list = ["item1", "item2", "item3"];
    res.json(list);
    console.log('Sent list of items');
});

// Handles any requests that don't match the ones above
app.get('*', (req,res) =>{
    res.sendFile(path.join(__dirname+'/client/build/index.html'));
});


app.listen(PORT, ()=>{
    console.log('Hello, you are running on '+PORT+ ' right now.')
})
